# veilid-node



## Prerequisites

* [Docker](https://docs.docker.com/engine/install/https://docs.docker.com/engine/install/)
* [Earthly](https://earthly.dev/)
* [Zarf](https://docs.zarf.dev/docs/getting-started) (optional if building/deploying as a zarf package)

## Configuring veilid-server

Edit the configuration file [veilid-server.conf](./veilid-server/veilid-server.conf) prior to deployment.

## Building docker image

```
earthly +build-dockerhub-image
```

## Pushing docker image 

```
earthly --push +build-dockerhub-image
```

## Deploying helmchart to current k8s context

```
helm install veilid-node .
```

## Packaging zarf package

(NOTE: zarf packaging and use case currently being debugged/testing)
```
cd zarf;
zarf package create
```

## Deploying zarf package

```
cd zarf;
zarf package deploy
```
