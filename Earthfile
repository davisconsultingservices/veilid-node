VERSION 0.7

FROM debian:stable-slim

# Install build prerequisites & setup required directories
deps-base:

    # Install necessary dependencies
    RUN apt-get -y update
    RUN apt-get -y install wget gnupg2 net-tools ca-certificates && \
        rm -rf /var/lib/apt/lists/*

    # Install Veilid Repository
    RUN wget -O- https://packages.veilid.net/gpg/veilid-packages-key.public | gpg --dearmor -o /usr/share/keyrings/veilid-packages-key.gpg
    RUN echo "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/veilid-packages-key.gpg] https://packages.veilid.net/apt stable main" > /etc/apt/sources.list.d/veilid-packages.list

    # Install Veilid Server and CLI
    RUN apt-get -y update && \
        apt-get -y install veilid-server veilid-cli && \
        apt-get autoclean

build-dockerhub-image:
    FROM +deps-base

    # Copy Veilid configuration file
    COPY ./docker/veilid-server.conf /etc/veilid-server/veilid-server.conf

    # Set the entrypoint
    ENTRYPOINT ["/bin/sh", "-c", "/usr/bin/veilid-server -c /etc/veilid-server/veilid-server.conf" ]

    SAVE IMAGE --push davisconsultingservices/veilid-node:latest
